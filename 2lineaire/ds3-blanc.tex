%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2024 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename

\documentclass[11pt]{article}

\usepackage[
  a5paper,
  includehead,
  margin=5mm,
  headsep=3mm,
]{geometry}

\usepackage{2324-pablo}
\usepackage{2324-pablo-paternault}
\usepackage{2324-pablo-math}
\usepackage{2324-pablo-tikz}

\usepackage{2324-pablo-header}
\fancyhead[L]{\textsc{Math 1\iere{} tronc commun}}
\fancyhead[R]{\textsc{DS \no3 --- Sujet blanc}}
\fancyfoot[C]{}

\begin{document}

\begin{exercice}
	Sur le site web des Hôpitaux Universitaires de Genève \url{https://www.hug.ch}, on trouve une infographie\footnote{Calendrier de grossesse \url{https://www.hug.ch/sites/interhug/files/structures/obstétrique/documents/calendrier_grossesse_7.pdf}}, présentant la taille et le poids du fœtus en fonction du nombre de semaines d'aménorrhée (date des dernières règles).

	\begin{center}\begin{tabular}{r*{8}{n{4}{0}}}
		\toprule
		Nombre de \\
		semaines & 9 & 13 & 16 & 20 & 25 & 31 & 35 & 38 \\
		\midrule
		Taille (cm) & 3 & 6 & 13 & 21 & 31 & 37 & 42 & 50 \\
		Poids (g) & 9 & 90 & 200 & 500 & 750 & 1750 & 2950 & 3600 \\
		\bottomrule
	\end{tabular}\end{center}

	\begin{enumerate}
		\item Représenter la taille et le poids du fœtus en fonction du temps sur les graphiques suivants.
	\end{enumerate}

			\begin{tikzpicture}[ultra thick, xscale=.12, yscale=.1]
				\draw[xstep=5, ystep=5, dotted] (0, 0) grid (40, 50);
				\draw[-latex] (0, 0) node[below left]{0} -- (43, 0) node[below left=12]{Temps (semaines)};
				\foreach \x in {10, 20, ..., 40} {
					\draw (\x, 0) node[below]{$\x$};
				}
				\foreach \y in {10, 20, ..., 50} {
					\draw (0, \y) node[left]{$\y$};
				}
				\draw[-latex] (0, 0) -- (0, 54) node[right]{Taille (cm)};;
			\end{tikzpicture}
			\hfill
			\begin{tikzpicture}[ultra thick, xscale=.12, yscale=.12]
				\draw[xstep=5, ystep=5, dotted] (0, 0) grid (40, 40);
				\draw[-latex] (0, 0) node[below left]{0} -- (43, 0) node[below left=12]{Temps (semaines)};
				\foreach \x in {10, 20, ..., 40} {
					\draw (\x, 0) node[below]{$\x$};
				}
				\foreach \y in {10, 20, ..., 40} {
					\draw (0, \y) node[left]{$\numprint{\y00}$};
				}
				\draw[-latex] (0, 0) -- (0, 44) node[right]{Poids (g)};
			\end{tikzpicture}

	\begin{enumerate}[resume]
		\item La taille et le poids d'un fœtus en fonction du temps suivent-ils une croissance linéaire ? Justifiez en vous appuyant sur les graphiques.
	\end{enumerate}
\end{exercice}

\newpage

\begin{exercice}
	On compare les prix proposées par deux compagnies de taxi.

	\begin{enumerate}
		\item La première compagnie, A, pratique un prix fixe de cinq euros, auquel on ajoute cinquante centimes par kilomètre parcouru. On admet que le prix, pour une distance parcourue $x$, est donné par la fonction $f$ définie sur $\mathbb{R}$ par :
			\[f(x)=0,5x+5\]
			\begin{enumerate}
				\item Quel est le prix d'une course de \qty{2}{km} ? de \qty{42}{km} ?
				\item Quelle est la nature de la fonction $f$ ?
				\item Quelle sont les variations de la fonction $f$ ? Comment aurait-on pu le prédire grâce au contexte de l'exercice ?
				\item Max n'a que \qty{13}{€} sur lui. Quelle est la distance maximale qu'il peut parcourir avec cette compagnie de taxi ?
			\end{enumerate}
		\item On sait que la seconde compagnie, B, pratique un prix de \qty{2,4}{€} pour une course de \qty{3}{km}, et de \qty{9,6}{€} pour une course de \qty{12}{km}. On admet que la fonction $g$, qui représente le prix à payer pour une distance de $x$ kilomètres, est une fonction affine.
			\begin{enumerate}
				\item Calculer le coefficient directeur de la fonction $g$.
				\item Montrer que l'ordonnée à l'origine de la fonction $g$ est 0.
			\end{enumerate}
		\item
			\begin{enumerate}
				\item Tracer sur le graphique suivant les deux courbes de $f$ et $g$ (en justifiant par le calcul, ou en laissant apparents les traits de construction).
			\end{enumerate}

			\begin{center}
			\begin{tikzpicture}[ultra thick, xscale=.35, yscale=.2]
				\draw[xstep=1, ystep=1, thin, dotted] (0, 0) grid (30, 20);
				\draw[xstep=5, ystep=5, dotted] (0, 0) grid (30, 20);
				\draw[-latex] (0, 0) node[below left]{0} -- (31, 0) node[below left=12]{Distance (km)};
				\foreach \x in {5, 10, ..., 30} {
					\draw (\x, 0) node[below]{$\x$};
				}
				\foreach \y in {5, 10, ..., 20} {
					\draw (0, \y) node[left]{$\y$};
				}
				\draw[-latex] (0, 0) -- (0, 22) node[above left=12, rotate=90]{Prix (€)};
			\end{tikzpicture}
			\end{center}
				On observe que la compagnie B est moins chère pour des petites distances.
			\begin{enumerate}[resume]
				\item Avec la précision permise par le graphique, donner la distance à partir de laquelle la compagnie A est moins chère ?
				\item Répondre à la même question par le calcul, en arrondissant au mètre près.
			\end{enumerate}
	\end{enumerate}
\end{exercice}

\end{document}
