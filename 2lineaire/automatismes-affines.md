- Tracer la courbe d'une fonction affine à partir de son expression
  
- Déterminer graphiquement l'expression d'une fonction affine
  https://www.youtube.com/watch?v=E0NTyDRqWfM

- Déterminer une fonction affine par 2 nombres et leurs images
  https://www.youtube.com/watch?v=cXl6snfEJbg