%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2021-2025 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename

\documentclass[12pt]{article}

\usepackage{2425-pablo}
\usepackage{2425-pablo-math}
\usepackage{2425-pablo-paternault}
\usepackage{2425-pablo-tikz}

\usepackage[
  a5paper,
  includehead,
  margin=5mm,
  headsep=3mm,
]{geometry}
\usepackage{2425-pablo-header}
\fancyhead[L]{\textsc{Chapitre 2 --- Croissance linéaire}}
\fancyhead[R]{\textsc{2 --- Suites arithmétiques}}

\usepackage{tabularray}
\usepackage{multicol}

\begin{document}

\begin{activite*}
  On définit la suite $u$ à l'aide des schémas suivants.

  \begin{center}
    \begin{tblr}{*{3}{c}}
      \begin{tikzpicture}[scale=.5,very thick, baseline={(0, 0)}]
          \draw[dashed] (0, 0) grid (2, 1);
        \end{tikzpicture}
        &
        \begin{tikzpicture}[scale=.5,very thick, baseline={(0, 0)}]
          \draw (0, 0) grid (2, 1);
          \draw[dashed] (-1, -1) grid (3, 2);
        \end{tikzpicture}
        &
        \begin{tikzpicture}[scale=.5,very thick, baseline={(0, 0)}]
          \draw (-1, -1) grid (3, 2);
          \draw[dashed] (-2, -2) grid (4, 3);
        \end{tikzpicture}
        %&
        %\begin{tikzpicture}[scale=.5,very thick, baseline={(0, 0)}]
        %  \draw (-2, -2) grid (4, 3);
        %  \draw[dashed] (-3, -3) grid (5, 4);
        %\end{tikzpicture}
        \\
        $u(1)=2$&
        $u(2)=10$&
        $u(3)=18$\\
        %$u(4)=26$\\
    \end{tblr}
  \end{center}

  \begin{enumerate}
    \item Compléter :
      \begin{enumerate}
        \item
            $u(4)=\ldots$ ;
            $u(5)=\ldots$ ;
            $u(10)=\ldots$.
          \item $u(\ldots)=98$.
          \item
            $u(0)=\ldots$ ;
            $u(-2)=\ldots$ ;
            $u(4,5)=\ldots$.
      \end{enumerate}
    \item On affirme que $u(26)=202$. Compléter :

      $u(27)=\ldots$ ;
      $u(25)=\ldots$ ;
      $u(30)=\ldots$.
    \item Pour une certaine valeur de $n$ inconnue, on a : $u(n)=530$. Compléter :

      $u(n+1)=\ldots$ ;
      $u(n+2)=\ldots$ ;
      $u(n-1)=\ldots$ ;
      $u(n)+1=\ldots$.
    \item
      \begin{enumerate}
        \item Sur le graphique suivant, placer les points correspondant à $u(1)$, $u(2)$ jusqu'à $u(5)$.
          \begin{center}
            \begin{tikzpicture}[xscale=1,yscale=.1,very thick]
              \draw[-latex] (0, 0) -- (8.5, 0);
              \draw[-latex] (0, 0) -- (0, 65);
              \draw[dotted] (0, 0) grid[xstep=1, ystep=5] (8, 60);
              \draw (0, 0) node[below left]{$0$};
              \foreach \x in {1, 2, ..., 8}{
                \draw (\x, 0) node[below]{$\x$};
              }
              \foreach \y in {10, 20, ..., 60}{
                \draw (0, \y) node[left]{$\y$};
              }
            \end{tikzpicture}
          \end{center}
        \item Compléter par lecture graphique :

          $u(6)=\ldots$ ;
          $u(7)=\ldots$ ;
          $u(8)=\ldots$.
      \end{enumerate}
  \end{enumerate}
  
\end{activite*}

\pagebreak

\section{Suites numériques}

\begin{definition*}
  On apelle \blanc{suite numérique} une suite finie ou infinie de nombres, appelés \blanc{termes de la suite}. Cette suite est habituellement notée $u$, $v$ ou $w$. Le premier terme est le plus souvent $u_0$ ou $u_1$, et pour un nombre entier $n$, $u_n$ est \blanc{le terme de rang $n$}.

  $u_{n+1}$ est le terme qui suit $u_n$, et $u_{n-1}$ est le terme qui précède $u_n$.
\end{definition*}

\begin{exemple}\label{ex:termes}~
  \begin{enumerate}
    \item On considère $u$ la suite de premier terme $u_0=8$, et dont chaque terme (sauf le premier) est égal à la moitié du précédent.

      \begin{enumerate*}
        \item Calculer $u_3$.
        \item Calculer le 5\ieme{} terme.
        \item Calculer le terme de rang 2.
      \end{enumerate*}
    \item On considère $v$ la suite définie pour tout nombre entier $n\geq1$ par $v_n=2n^2-3$.

      \begin{enumerate*}
        \item Calculer $v_3$.
        \item Calculer le 4\ieme{} terme.
        \item Calculer le terme de rang 2.
      \end{enumerate*}
    \item On considère $w$ la suite définie par :
      \begin{equation*}
        \left\{\begin{aligned}
            w_0 &= 5\\
            w_{n+1} &= 2w_n-4 \text{ pour tout $n$ entier positif.}\\
        \end{aligned}\right.
      \end{equation*}

      \begin{enumerate*}
        \item Calculer $w_3$.
        \item Calculer le 4\ieme{} terme.
        \item Calculer le terme de rang 2.
      \end{enumerate*}
  \end{enumerate}
\end{exemple}

\begin{remarque*}
  Le module \emph{Suites} de la calculatrice permet de manipuler les suites.
\end{remarque*}

\begin{exemple}
  Reprendre les suites $u$, $v$, $w$ de l'exemple \ref{ex:termes}, et calculer $u_{100}$, $v_{100}$, $w_{100}$.
\end{exemple}

\begin{definition*}[Variations]
  Une suite $u$ est dite :
  \begin{itemize}
    \item \emph{croissante} si chaque terme est plus grand que le précédent : $u_{n+1}\geq u_n$ ;
    \item \emph{constante} si chaque terme est égal aux précédent : $u_{n+1}= u_n$ ;
    \item \emph{décroissante} si chaque terme est plus petit que le précédent : ${u_{n+1}\leq u_n}$ ;
  \end{itemize}
\end{definition*}


\begin{definition*}[Représentation graphique]
  La représentation graphique d'une suite $u$ est le nuage de points de coordonnées $\left( n;u_n \right)$.
\end{definition*}

\begin{exemple}\label{ex:graph}
  On définit la suite $u$ sur $\mathbb{N}$ par :
  \begin{equation*}
    \left\{\begin{aligned}
        u_0 &= 0,5\\
        u_{n+1} &= 0,75u_n+1 \text{ pour tout $n$ entier positif.}\\
    \end{aligned}\right.
  \end{equation*}
  \begin{enumerate}
    \item À l'aide de la calculatrice, déterminer les onze premiers termes de la suite (arrondir au dixième).
    \item Tracer la suite sur le graphique ci-dessous.
      \begin{center}
    \begin{tikzpicture}[scale=1,very thick]
      \begin{axis}[
          yscale=.5,
          xscale=1.6,
          xmin=0,
          xmax=10,
          ymin=0,
          ymax=4,
          ultra thick,
          axis lines=middle,
          enlargelimits=upper,
          %minor x tick num=4,
          minor y tick num=3,
          xtick={0, 1, ..., 10},
          ytick={0, 1, ..., 4},
          major grid style={black, thick},
          grid=both,
          %xlabel={$n$},
          %ylabel={$u(n)$},
          clip=false,
        ]
        \draw (axis cs:0, 0) node[below left]{$0$};
      \end{axis}
    \end{tikzpicture}%
  \end{center}
  \end{enumerate}
\end{exemple}

\begin{exemple}
  Par lecture graphique, conjecturer le sens de variation de la suite $u$ de l'exemple \ref{ex:graph}.
\end{exemple}

\section{Suites arithmétiques}

\begin{activite*}
  L'activité d'une entreprise étant florissante, en moyenne 7 nouveaux employés ont été embauchés chaque année. En 2021, elle comptait 38 employés, et on suppose que cette progression va se poursuivre dans les années à venir.

  On appelle $u_n$ le nombre d'employés l'année $2021+n$.

  \begin{enumerate}
    \item Donner les cinq premiers termes de la suite.
    \item Combien y aura-t-il d'employés en 2035 ?
    \item Exprimer $u_{n+1}$ en fonction de $u_n$, pour tout $n\in\mathbb{N}$.
  \end{enumerate}
\end{activite*}

Une suite est arithmétique si on passe au terme suivant en ajoutant (ou soustrayant) toujours le même nombre.

\begin{definition*}
  Une suite $u$ est dite \blanc{arithmétique} s'il existe un réel $r$, appelé \blanc{raison}, tel que pour tout $n\in\mathbb{N}$, on ait : \blanc{$u_{n+1}=u_n+r$}.

  Habituellement, une suite arithmétique est définie par la donnée de \blanc{son premier terme et sa raison}.
\end{definition*}

\begin{exemple}\label{ex:calcul}~
  \begin{enumerate}
    \item Donner les cinq premiers termes de la suite arithmétique $u$ de premier terme $u_0=13$ et de raison \numprint{-2.5}.
    \item La suite $v$ est arithmétique, et on sait que  $v_3=1$ et $v_4=3$. Déterminer la raison, puis calculer $v_5$, $v_8$, et $v_2$..
  \end{enumerate}
\end{exemple}

\begin{exemple}\label{ex:graph}
  Représenter graphiquement (de couleur différente) les deux suites de l'exemple \ref{ex:calcul}. Que remarquez-vous ?
  \begin{center}
    \begin{tikzpicture}[scale=1,very thick]
      \begin{axis}[
          yscale=.5,
          xscale=1.6,
          xmin=0,
          xmax=5,
          ymin=0,
          ymax=12,
          ultra thick,
          axis lines=middle,
          enlargelimits=upper,
          %minor x tick num=4,
          minor y tick num=1,
          %xtick={0, 1, ..., 10},
          %ytick={0, 1, ..., 10},
          major grid style={black, thick},
          grid=both,
          %xlabel={$n$},
          %ylabel={$u(n)$},
          clip=false,
        ]
        \draw (axis cs:0, 0) node[below left]{$0$};
      \end{axis}
    \end{tikzpicture}%
  \end{center}
\end{exemple}

\begin{propriete*}
  Une suite est arithmétique si et seulement si les points de sa représentation graphique sont alignés.

  On dit que les termes de la suite suivent un modèle de \blanc{croissance linéaire}.
\end{propriete*}

\begin{exercice*}
  Plan de travail n\no 1.
\end{exercice*}

\begin{propriete*}
  La suite $u$ est :
  \begin{itemize}
    \item \blanc{croissante} si et seulement si sa raison est \emph{positive} ;
    \item \blanc{décroissante} si et seulement si sa raison est \emph{négative} ;
    \item \blanc{constante} si sa raison est \emph{nulle}.
  \end{itemize}
\end{propriete*}

\begin{exemple}
  Déterminer les variations des deux suites $u$ et $v$ de l'exemple \ref{ex:calcul}.
\end{exemple}

\begin{propriete*}~
  \begin{itemize}
    \item Pour tout $n$ et $p$ de son domaine de définition, on a : \[\blanc{u_n=u_p+(n-p)r}\]
    \item En particulier, si $u$ est définie sur $\mathbb{N}$, pour tout $n\in\mathbb{N}$, on a : \[\blanc{u_n=u_0+nr}\]
  \end{itemize}
\end{propriete*}

\begin{exemple}
  On reprend les suites $u$ et $v$ de l'exemple \ref{ex:calcul}.
  \begin{enumerate}
    \item Déterminer $u_{100}$ et $v_{100}$.
    \item Déterminer le plus petit nombre $n$ tel que $v_n\geq1000$.
  \end{enumerate}
\end{exemple}

\end{document}
