%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2024 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename

\documentclass[12pt]{article}

\usepackage[
  a5paper,
  includehead,
  margin=5mm,
  headsep=3mm,
]{geometry}

\usepackage{2324-pablo}
\usepackage{2324-pablo-paternault}
\usepackage{2324-pablo-math}
\usepackage{2324-pablo-tikz}

\usepackage{2324-pablo-header}
\fancyhead[L]{\textsc{Math 1\iere{} tronc commun}}
\fancyhead[R]{\textsc{DS \no3 --- Sujet blanc}}
\fancyfoot[C]{}

\begin{document}

\begin{exercice}
  \begin{em}
    Sur le site web des Hôpitaux Universitaires de Genève \url{https://www.hug.ch}, on trouve une infographie\footnote{Calendrier de grossesse \url{https://www.hug.ch/sites/interhug/files/structures/obstétrique/documents/calendrier_grossesse_7.pdf}}, présentant la taille et le poids du fœtus en fonction du nombre de semaines d'aménorrhée (date des dernières règles).
  \end{em}

  \begin{center}\begin{tabular}{r*{8}{n{4}{0}}}
      \toprule
      Nombre de \\
      semaines & 9 & 13 & 16 & 20 & 25 & 31 & 35 & 38 \\
      \midrule
      Taille (cm) & 3 & 6 & 13 & 21 & 31 & 37 & 42 & 50 \\
      Poids (g) & 9 & 90 & 200 & 500 & 750 & 1750 & 2950 & 3600 \\
      \bottomrule
  \end{tabular}\end{center}

  \begin{enumerate}
    \item \emph{Représenter la taille et le poids du fœtus en fonction du temps sur les graphiques suivants.}
  \end{enumerate}

  \begin{tikzpicture}[ultra thick, xscale=.12, yscale=.1]
    \draw[xstep=5, ystep=5, dotted, gray] (0, 0) grid (40, 50);
    \draw[-latex] (0, 0) node[below left]{0} -- (43, 0) node[below left=12]{Temps (semaines)};
    \foreach \x in {10, 20, ..., 40} {
      \draw (\x, 0) node[below]{$\x$};
    }
    \foreach \y in {10, 20, ..., 50} {
      \draw (0, \y) node[left]{$\y$};
    }
    \draw[-latex] (0, 0) -- (0, 54) node[right]{Taille (cm)};;
    \foreach \x/\y in {9/3, 13/6, 16/13, 20/21, 25/31, 31/37, 35/42, 38/50} {
      \draw (\x, \y) node{\times};
    }
  \end{tikzpicture}
  \hfill
  \begin{tikzpicture}[ultra thick, xscale=.12, yscale=.12]
    \draw[xstep=5, ystep=5, dotted, gray] (0, 0) grid (40, 40);
    \draw[-latex] (0, 0) node[below left]{0} -- (43, 0) node[below left=12]{Temps (semaines)};
    \foreach \x in {10, 20, ..., 40} {
      \draw (\x, 0) node[below]{$\x$};
    }
    \foreach \y in {10, 20, ..., 40} {
      \draw (0, \y) node[left]{$\numprint{\y00}$};
    }
    \draw[-latex] (0, 0) -- (0, 44) node[right]{Poids (g)};
    \foreach \x/\y in {9/.09, 13/.9, 16/2, 20/5, 25/7.5, 31/17.5, 35/29.5, 38/36} {
      \draw (\x, \y) node{\times};
    }
  \end{tikzpicture}

  \begin{enumerate}[resume]
    \item \emph{La taille et le poids d'un fœtus en fonction du temps suivent-ils une croissance linéaire ? Justifiez en vous appuyant sur les graphiques.}

      Pour la taille, les deux réponses suivantes sont acceptables (à condition d'être correctement justifiées).

      \begin{enumerate}
        \item Les points ne sont pas alignés, donc la croissance n'est pas linéaire (c'est l'application \emph{stricte} de la linéarité).
        \item Les points sont à peu près alignés, donc la croissance est presque linéaire (puisque les données \emph{réelles} ne sont jamais \emph{parfaitement} linéaires, on s'autorise cette approximation).
      \end{enumerate}

      Puisque les points ne sont pas alignés, le poids ne suit pas une croissance linéaire.
  \end{enumerate}
\end{exercice}

\begin{exercice}
  \begin{em}
    On compare les prix proposées par deux compagnies de taxi.
  \end{em}

  \begin{enumerate}
    \item \emph{La première compagnie, A, pratique un prix fixe de cinq euros, auquel on ajoute cinquante centimes par kilomètre parcouru. On admet que le prix, pour une distance parcourue $x$, est donné par la fonction $f$ définie sur $\mathbb{R}$ par :}
      \[f(x)=0,5x+5\]
      \begin{enumerate}
        \item \emph{Quel est le prix d'une course de \qty{2}{km} ? de \qty{42}{km} ?}

          \begin{enumerate}
            \item Course de \qty{2}{km} : $f(2)=0,5\times2+5=$\qty{6}{€}.
            \item Course de \qty{42}{km} : $f(42)=0,5\times2+45=$\qty{26}{€}.
          \end{enumerate}
        \item \emph{Quelle est la nature de la fonction $f$ ?}

          La fonction $f$ est une fonction affine, de coefficient directeur $0,5$, et d'ordonnée à l'origine $5$.
        \item \emph{Quelle sont les variations de la fonction $f$ ? Comment aurait-on pu le prédire grâce au contexte de l'exercice ?}

          C'est une fonction affine de coefficient directeur positif : elle est croissante.

          C'est normal, puisque cette fonction représente le prix d'une course en fonction de la distance : plus la distance est grande, plus le prix est élevé.
        \item \emph{Max n'a que \qty{13}{€} sur lui. Quelle est la distance maximale qu'il peut parcourir avec cette compagnie de taxi ?}

          Résolvons l'inéquation suivante :

          \begin{align*}
            f(x)&\leq13\\
            0,5x+5&\leq13\\
            0,5x&\leq8\\
            x&\leq\frac{8}{0,5}\\
            x&\leq16
          \end{align*}

          Donc la distance maximale que Max peut parcourir est \qty{16}{km}.
      \end{enumerate}
    \item \emph{On sait que la seconde compagnie, B, pratique un prix de \qty{2,4}{€} pour une course de \qty{3}{km}, et de \qty{9,6}{€} pour une course de \qty{12}{km}. On admet que la fonction $g$, qui représente le prix à payer pour une distance de $x$ kilomètres, est une fonction affine.}
      \begin{enumerate}
        \item \emph{Calculer le coefficient directeur de la fonction $g$.}

          Les données de l'énoncé peuvent être réécrites comme $g(3)=2,4$ et $g(12)=9,6$. Donc le coefficient directeur est :

          \[
            \frac{g(12)-g(3)}{12-3}=\frac{9,6-2,4}{9}=\frac{7,2}{9}=0,8
          \]
        \item \emph{Montrer que l'ordonnée à l'origine de la fonction $g$ est 0.}

          La fonction $g$ est affine, de coefficient directeur $0,8$, donc son expression est $g(x)=0,8x+b$ (où $b$ est l'ordonnée à l'origine, à déterminer). Or on sait que $g(3)=2,4$, donc :

          \begin{align*}
            g(3)&=2,4\\
            0,8\times3+b&=2,4\\
            2,4+b&=2,4\\
            b&=0
          \end{align*}
          Donc l'ordonnée à l'origine de $g$ est 0, et l'expression de $g$ est : $g(x)=2,4x$.
      \end{enumerate}
    \item
      \begin{enumerate}
        \item \emph{Tracer sur le graphique suivant les deux courbes de $f$ et $g$ (en justifiant par le calcul, ou en laissant apparents les traits de construction).}
      \end{enumerate}

      \begin{center}
        \begin{tikzpicture}[ultra thick, xscale=.35, yscale=.2]
          \draw[xstep=1, ystep=1, thin, dotted] (0, 0) grid (30, 20);
          \draw[xstep=5, ystep=5, dotted] (0, 0) grid (30, 20);
          \draw[-latex] (0, 0) node[below left]{0} -- (31, 0) node[below left=12]{Distance (km)};
          \foreach \x in {5, 10, ..., 30} {
            \draw (\x, 0) node[below]{$\x$};
          }
          \foreach \y in {5, 10, ..., 20} {
            \draw (0, \y) node[left]{$\y$};
          }
          \draw[-latex] (0, 0) -- (0, 22) node[above left=12, rotate=90]{Prix (€)};
          \draw (0, 5) -- (30, {.5*30+5}) node[right]{$\mathcal{C}_f$};
          \draw (0, 0) -- (25, {.8*25}) node[above]{$\mathcal{C}_g$};
          \begin{scope}[blue]
          \draw[dashed] (0, {50/3*.8}) node[left=15]{$\approx 13,3$} -- ++(50/3, 0) -- (50/3, 0) node[below=10]{$\approx 16,7$};
        \end{scope}
        \end{tikzpicture}
      \end{center}
      \begin{em}
        On observe que la compagnie B est moins chère pour des petites distances.
      \end{em}
      \begin{enumerate}[resume]
        \item \emph{Avec la précision permise par le graphique, donner la distance à partir de laquelle la compagnie A est moins chère ?}
        \item \emph{Répondre à la même question par le calcul, en arrondissant au mètre près.}

          On cherche à résoudre $f(x)\geq g(x)$ :
          \begin{align*}
            f(x)&\geq g(x)\\
            0,5x+5&\geq0,8x\\
            0,5x-0,8x+5&\geq0\\
            -0,3x+5&\geq0\\
            -0,3x&\geq-5\\
            x&\leq\frac{-5}{-0,3}\\
            x&\leq\frac{50}{3}\\
          \end{align*}
          Or $\frac{50}{3}\approx 16,667$, donc la compagnie de taxi A est moins chère que la B à partir d'environ \qty{16,667}{km}.
      \end{enumerate}
  \end{enumerate}
\end{exercice}

\end{document}
