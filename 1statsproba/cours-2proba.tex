%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2021-2024 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename

\documentclass[twoside, 12pt]{article}

\usepackage{2324-pablo}
\usepackage{2324-pablo-math}
\usepackage{2324-pablo-paternault}
\usepackage{2324-pablo-tikz}

\usepackage[
  a5paper,
  includehead,
  margin=5mm,
  headsep=3mm,
]{geometry}
\usepackage{2324-pablo-header}
\fancyhead[OR]{\textsc{Chapitre 1 --- Statistiques et Probabilités}}
\fancyhead[EL]{\textsc{2 --- Probabilités}}
\renewcommand{\thesection}{\alph{section}} 

\usepackage{tabularray}
\UseTblrLibrary{booktabs}

\usepackage{titlesec}
\titleformat{\section}{\normalfont\large\bfseries}{\thesection}{1em}{}

\usepackage{multicol}

\begin{document}

\section{Fréquences marginales, fréquences conditionnelles}

\begin{definition*}
  Dans un tableau croisé d'effectifs :
  \begin{itemize}
    \item la valeur à l'intersection d'une ligne et d'une colonne est le nombre d'individus présentant simultanément les deux caractères ;
    \item on appelle \blanc{effectif marginal} l'effectif d'une sous-population qui ne dépend que d'un seul caractère. Ces effectifs sont représentés dans la ligne \enquote{Total} ou la colonne \enquote{Total} ;
    \item la \blanc{fréquence marginale} d'une valeur est le quotient :
      \[\dfrac{\text{Effectif marginal}}{\text{Effectif total}}\]
    \item la \blanc{fréquence conditionnelle} de la valeur $x_i$ sachant $y_j$ est le quotient :
      \[\dfrac{\text{Effectif présentant les valeurs $x_i$ et $y_j$}}{\text{Effectif marginal de la valeur $y_j$}}\]
  \end{itemize}
\end{definition*}

\begin{exemple*}
  Une association récupère des vélos jetés à la déchetterie pour éventuellement les remettre en état. Ces vélo sont de deux types : adulte ou enfant. Leur état est classé en trois catégories :
  bon état (prêts à rouler) ;
  réparable (peuvent être remis en état en moins de deux heures) ;
  non réparable (trop de réparation à faire).
  Voici le nombre de vélos traités en un mois.
  \begin{center}
    \begin{tblr}{|r*{4}{|X[c,b]}|}
      \hline
      & Bon état & Réparable & Non réparable & Total \\
      \hline
      Adulte & 7 & 26 & 12 & \\
      \hline
      Enfant &  & 18 & 5 & 26 \\
      \hline
      Total &&&& \\
      \hline
    \end{tblr}
  \end{center}

  \begin{enumerate}
    \item Complétez le tableau.
    \item Parmi les vélos réparable, combien sont des vélos adultes ?
    \item Calculez la fréquence marginale de vélos en bon état parmi l'ensemble des vélos de l'association.
    \item Calculez la fréquence conditionnelle de vélos enfant parmi les vélos en bon état.
  \end{enumerate}
\end{exemple*}

\section{Probabilités conditionnelles}

\begin{definition*}[Rappels]
  Étant donnés deux évènements $A$ et $B$ d'un univers $\Omega$ :
  \begin{itemize}[$\bullet$]
    \item l'intersection de $A$ et $B$, notée $A\cap B$, est \blanc{l'ensemble des éléments qui appartiennent à la fois à $A$ et à $B$} ;
    \item l'union de $A$ et $B$, notée $A\cup B$, est \blanc{l'ensemble des éléments qui appartiennent à $A$, ou à $B$, ou aux deux à la fois} ;
    \item le contraire de $A$, noté $\overline{A}$, est \blanc{l'ensemble des issues de l'univers qui n'appartiennent pas à $A$}.
  \end{itemize}
\end{definition*}

\begin{exemple}On considère les élèves de cette classe, et :
  \begin{itemize}[$\bullet$]
    \item \evenement{A}{l'ensemble des garçons} ;
    \item \evenement{B}{l'ensemble des personnes portant des lunettes}.
  \end{itemize}
  Alors :
  \begin{itemize}[$\bullet$]
    \item $A\cup B$ est : \blanc{l'ensemble des garçons à lunettes}.
    \item $\overline{B}$ est : \blanc{l'ensemble des personnes sans lunettes}.
    \item $A\cap\overline{B}$ est : \blanc{l'ensemble des garçons sans lunettes}.
  \end{itemize}
\end{exemple}

%\begin{exemple}
%  On lance un dé à 20 faces numérotées de 1 à 20, et on observe le dé obtenu. On note les évènements suivants :
%  \begin{itemize}[$\bullet$]
%    \item \evenement{I}{le nombre obtenu est impair} ;
%    \item \evenement{T}{le nombre obtenu est un multiple de 3}.
%  \end{itemize}
%  Énumérez les évènements de :
%  \begin{itemize}[$\bullet$]
%    \item $I$ : \dotfill
%    \item $T$ : \dotfill
%    \item $I\cap T$ : \dotfill
%  \end{itemize}
%\end{exemple}

\begin{definition*}[Rappel]
  On choisi de manière équiprobable un individu au hasard dans une population, et on considère un évènement $A$. Alors :
  \[
    P(A)=\blanc{\frac{\text{Nombre de cas favorables}}{\text{Nombre de cas possibles}}}
  \]
\end{definition*}

\begin{exemple}
  Pour vérifier la qualité d'une chaîne de montage d'aspirateurs, une équipe vérifie leur fonctionnement : sur 67 aspirateurs étudiés, 3 ne fonctionnent pas.

  On choisit un aspirateur au hasard à la sortie de cette chaîne de montage. Quelle est la probabilité qu'il ne fonctionne pas ?
\end{exemple}

\begin{defprop*}Soient $A$ et $B$ deux évènements d'une expérience aléatoire, tels que $P(B)\neq0$.

      On appelle \blanc{probabilité conditionnelle de $A$ sachant $B$}, ou \enquote{probabilité que $A$ soit réalisé sachant que $B$ est réalisé}, le nombre :
      \[P_B\left( A \right)=\frac{\card(A\cap B)}{\card(B)}=\frac{P\left( A\cap B \right)}{P\left( B \right)}\]
\end{defprop*}

\pagebreak

\begin{exemple}
  \emph{Inspiré du sujet d'E3C série technologique, série 2, n\no49.}
Une usine produit et vend de l'eau minérale en bouteilles d'un litre. L'eau provient de deux sources A et B.

  Un laboratoire indépendant effectue des tests sur un stock journalier de 400 bouteilles produites par l'usine et détermine si l'eau est calcaire ou non :

  \begin{itemize}[$\bullet$]
    \item 250 bouteilles provenant de la source A ont été testées, parmi lesquelles 12 contenaient de l'eau calcaire.
    \item 85 \% des bouteilles testées ne contenaient pas d'eau calcaire.
  \end{itemize}

  \begin{enumerate}
    \item Compléter le tableau suivant :
      \begin{center}
        \begin{tblr}{|r*{3}{|X[c,b]}|}
          \hline
          & Source A & Source B & Total \\
          \hline
          Eau calcaire & & & \\
          \hline
          Eau non calcaire & & & \\
          \hline
          Total &&& 400 \\
          \hline
        \end{tblr}
      \end{center}
    \item On choisit au hasard une bouteille parmi le stock des 400 bouteilles testées. Toutes les bouteilles du stock ont la même probabilité d'être choisies.

      On considère les évènements :
      \begin{itemize}[$\bullet$]
        \item\evenement{A}{la bouteille provient de la source A} ;
        \item\evenement{B}{la bouteille provient de la source B} ;
        \item\evenement{C}{l'eau contenue dans la bouteille est calcaire}.
      \end{itemize}
      \begin{enumerate}
        \item Calculer $P(A)$.
        \item Justifier que $P(C)=0,15$.
        \item Traduire par une phrase l'évènement $B\cap C$ puis calculer sa probabilité.
        \item Calculer la probabilité que l'eau contenue dans la bouteille provienne de la source B sachant qu'elle est calcaire.
      \end{enumerate}
  \end{enumerate}
\end{exemple}

\pagebreak

\begin{definition*}
  Deux évènements $A$ et $B$ (avec $P(B)\neq0$) sont dits \blanc{indépendants} si $P_B(A)=P(A)$.
\end{definition*}

\begin{propriete*}
  Deux évènements $A$ et $B$ sont \emph{indépendants} si et seulement si $P(A\cap B)=P(A)\times P(B)$.
\end{propriete*}

% TODO Ajouter un exemple

\section{Répétition d'expériences aléatoires}

\begin{propriete*}
  On peut modéliser la successions d'expériences aléatoires par un arbre de probabilités (ou arbre pondéré) respectant les règles suivantes.
  \begin{itemize}
    \item La somme des probabilités des branches issues d'un nœud est égale à 1.
    \item Le probabilité d'une issue d'un chemin est égale au produit des probabilités rencontrées sur le chemin.
    \item La probabilité d'un évènement est égale à la somme des probabilités des issues qui le composent.
  \end{itemize}
\end{propriete*}

% TODO Remplacer cet exemple avec un exemple avec des A, B, A barre, B barre

\begin{exemple}
  Une pièce truquée a une probabilité $^1/_3$ de tomber sur pile. On la lance deux fois de suite. Quelle est la probabilité d'obtenir au moins une fois pile ?
\end{exemple}

\begin{exemple}[Inspiré du sujet d'E3C \no44 --- Mai 2020]
  Une personne est choisie au hasard parmi les participants d'une étude scientifique.
  On note $M$ l'évènement \enquote{la personne est malade} et $S$ l'évènement \enquote{la personne a une activité sportive régulière}.

  \emph{Si nécessaire, les résultats approchés seront donnés à $10^{-3}$ près.}

  \begin{multicols}{2}
    \begin{enumerate}
      \item Compléter l'arbre pondéré ci-contre, représentant la situation.
      \item Décrire par une phrase la probabilité $P_{M}\left(\overline{S}\right)$, et donner sa probabilité.
    \end{enumerate}

    \begin{center}
      \begin{tikzpicture}[grow=right, sloped]
        % Set the overall layout of the tree
        \tikzstyle{level 1}=[level distance=2.5cm, sibling distance=8ex]
        \tikzstyle{level 2}=[level distance=2.5cm, sibling distance=3ex]
        \node {}
        child {
          node {$\overline{M}$}
          child {
            node {$\overline{S}$}
            edge from parent node[midway, below]{…}
          }
          child {
            node {$S$}
            edge from parent node[midway, above]{$0,54$}
          }
          edge from parent node[midway, below, sloped]{$0,88$}
        }
        child {
          node {$M$}
          child {
            node {$\overline{S}$}
            edge from parent node[midway, below]{…}
          }
          child {
            node {$S$}
            edge from parent node[midway, above, sloped]{$0,36$}
          }
          edge from parent node[midway, above, sloped]{…}
        };
      \end{tikzpicture}
    \end{center}
  \end{multicols}
  \begin{enumerate}[resume]
      \setcounter{enumi}{2}
    \item
      \begin{enumerate}
        \item Quelle est la probabilité que la personne soit malade et qu'elle pratique une activité sportive régulièrement ?
        \item Montrer que la probabilité que la personne pratique une activité sportive régulièrement est égale à \numprint{0,518}.
      \end{enumerate}
    \item La personne choisie a une activité sportive régulière. Quelle est la probabilité pour qu'elle soit malade ?
    \item Les évènements $M$ et $S$ sont-ils indépendants ?
    %\item Un journaliste annonce qu'une pratique régulière d'une activité sportive diminue par deux le risque de tomber malade. Que peut-on conclure sur la pertinence de cette annonce ?  Justifier.
  \end{enumerate}

\end{exemple}

\end{document}
