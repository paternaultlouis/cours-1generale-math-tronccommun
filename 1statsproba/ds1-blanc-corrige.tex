%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2024 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename

\documentclass[11pt]{article}

\usepackage{2425-pablo}
\usepackage{2425-pablo-paternault}
\usepackage{2425-pablo-math}
\usepackage{2425-pablo-tikz}

\usepackage[a5paper, includehead, headsep=3mm, margin=7mm]{geometry}
\usepackage{2425-pablo-header}
\fancyhead[L]{\textsc{1\iere{} math tronc commun}}
\fancyhead[R]{\textsc{DS \no1 --- Sujet blanc --- Corrigé}}
\fancyfoot[C]{}

\usepackage{xcolor}

\begin{document}

\begin{exercice}
  \begin{em}
  Une association récupère des vieux vélos, qu'elle répare et revend. Ces vélos sont classés en trois catégories : enfant, route, VTT. Les vélos récupérés sont considérés comme en bon état (peuvent être revendus quasiment en l'état), à réparer (des réparations importantes sont nécessaires), ou à jeter (le vélo est trop abîmé). Le tableau suivant présente les derniers vélos récupérés par l'association.

  \begin{center}\begin{tabular}{cccp{1cm}ccc}
      \toprule
      & Catégorie & État &&& Catégorie & État \\
      \cmidrule{1-3}\cmidrule{5-7}
      Vélo  1 & route & bon état && Vélo 11 & enfant & à réparer\\
      Vélo  2 & route & à jeter && Vélo 12 & VTT & à jeter\\
      Vélo  3 & enfant & bon état && Vélo 13 & enfant & à réparer\\
      Vélo  4 & route & à jeter && Vélo 14 & VTT & bon état\\
      Vélo  5 & route & à réparer && Vélo 15 & VTT & à jeter\\
      Vélo  6 & enfant & à jeter && Vélo 16 & route & à jeter\\
      Vélo  7 & VTT & à réparer && Vélo 17 & VTT & à réparer\\
      Vélo  8 & enfant & à jeter && Vélo 18 & route & bon état\\
      Vélo  9 & route & à jeter && Vélo 19 & VTT & à réparer\\
      Vélo 10 & enfant & bon état && Vélo 20 & route & à jeter\\
      \bottomrule
    \end{tabular}\end{center}
  \end{em}

    \begin{enumerate}
      \item \emph{Compléter le tableau croisé d'effectifs suivant avec les données du tableau précédent.}
        \begin{center}\begin{tabular}{r||c|c|c}
            & enfant & route & VTT \\
            \hline
            \hline
            bon état &2&2&1 \\
            \hline
            à réparer &2&1&3 \\
            \hline
            à jeter &2&5&2 \\
        \end{tabular}\end{center}
      \item \emph{Répondre aux questions suivantes en vous aidant du tableau précédent.}
        \begin{enumerate}
          \item \emph{Quelle est la proportion de vélos en bon état ?}

            Il y a au total 20 vélos, dont \(2+2+1=5\) en bon état. Donc la proportion de vélos en bon état est $\frac{5}{20}=\frac{1}{4}$, soit $0,25$ ou $25\%$ (chacune des trois écriture --- fraction, nombre décimal, pourcentage --- est correcte).
          \item \emph{Un employé affirme que parmi les vélos récupérés, les VTT sont généralement en plus mauvais état que les vélos de route. A-t-il raison ?}
            Calculons la proportion de vélos à jeter parmi les VTT et les vélos de route.

            \begin{description}
              \item[Vélos de route] Il y a \(2+1+5=8\) vélos de route, dont 5 à jeter, donc la proportion de vélos de route à jeter est $\frac{5}{8}=0,625$.
              \item[VTT] Il y a \(1+3+2=6\) VTT, dont 2 à jeter, donc la proportion de VTT à jeter est $\frac{2}{6}\approx0,33$.
            \end{description}
            Donc la proportion de VTT à jeter est inférieure à celle des vélos de route : les VTT sont en meilleur état, et l'employé a tort.

            Remarque : Nous aurions aussi pu calculer la proportion de vélos en bon état dans les deux cas, et comparer les proportions. La conclusion aurait été la même.
        \end{enumerate}
    \end{enumerate}
\end{exercice}

\begin{exercice}
  \begin{em}
  Lors d'une grève à la SNCF en 2018, l'institut de sondage Elabe a obtenu les réponses suivantes à la question \enquote{Que pensez-vous de la mobilisation des organisations syndicales contre le projet de réforme du gouvernement ?}.
  \begin{center}
    \begin{tabular}{ccc}
      \toprule
      Soutien & Opposition & Indifférent \\
      \midrule
      37\% & 48\% & 15\% \\
      \bottomrule
    \end{tabular}
  \end{center}
  \end{em}
  \begin{enumerate}
    \item \emph{On souhaite réaliser un diagramme circulaire pour représenter ces résultats. Quel sera l'angle de chacun des trois secteurs ?}

      Dressons le tableau de proportionnalité correspondant (les valeurs en rouge sont celles qui ont été calculées ; celles en noires étaient trouvées dans l'énoncé).

      \begin{center}
      \begin{tabular}{rcccc}
        \toprule
          & Soutien & Opposition & Indifférent & Total\\
          \midrule
          Pourcentage & 37 & 48 & 15 & \textcolor{red}{100} \\
          Angle & \textcolor{red}{133,2} & \textcolor{red}{172,8} & \textcolor{red}{54} & \textcolor{red}{360} \\
          \bottomrule
      \end{tabular}
    \end{center}
    \item \emph{À la suite de ce sondage, la chaîne d'information \textsc{BFMTV} a diffusé le graphique suivant. Commentez ce graphique.}
      \begin{center}
        \includegraphics[width=6cm]{ds1-blanc-bfm-camembert}
      \end{center}

      Le secteur \enquote{Opposition} devrait avoir un angle de \ang{172,8} (soit moins d'un demi-cercle : \ang{180}), il a sur le graphique de BFMTV un angle supérieur à \ang{180} (car plus d'un demi-cercle). Le graphique publié sur la chaîne est donc faux.
  \end{enumerate}
\end{exercice}

\begin{exercice}
  \begin{em}
  Chaque point du graphique ci-dessous correspond à un pays de l'Union Européenne\footnote{Source : Article \emph{États membres de l'Union européenne} de Wikipédia en français \url{https://fr.wikipedia.org/wiki/États_membres_de_l'Union_européenne}}, placé en fonction sa population et sa superficie.
\end{em}

\begin{tikzpicture}[thick, xscale=0.15, yscale=0.05]
  \draw[thin, gray, dotted, xstep=5, ystep=5] (0, 0) grid (70, 90);
  \draw[dotted, xstep=10, ystep=10] (0, 0) grid (70, 90);
  \draw[-latex] (0, 0) -- (70, 0) node[below=12, anchor=north east]{Superficie (milliers de \unit{km^2})};
\draw[-latex] (0, 0) -- (0, 90) node[left=25, rotate=90]{Population (millions)};
\foreach \x in {10, 20, ..., 60}{
	\draw (\x, 0) node[below]{\x0};
	\draw (\x, -1) -- (\x, 1);
}
\draw (0, 0) node[below left]{$0$};
\foreach \y in { 10, 20, ..., 80}{
  \draw (0, \y) node[left]{\y};
  \draw (-1, \y) -- (1, \y);
}
% Autriche
\draw (8.3871, 8.508) node{$\times$};
%\draw (8.3871, 8.508) node[above left]{AT};
% Belgique
\draw (3.0500, 11.204) node{$\times$};
%\draw (3.0500, 11.204) node[above left]{BE};
% Bulgarie
\draw (11.0910, 7.246) node{$\times$};
%\draw (11.0910, 7.246) node[above left]{BG};
% République tchèque
\draw (7.8870, 10.512) node{$\times$};
%\draw (7.8870, 10.512) node[above left]{CZ};
% Allemagne
\draw (35.7027, 83.13) node{$\times$};
\draw[dashed, red] (35.7027, 0) node[below]{360} -- ++(0, 83.13) -- ++(-35.7027, 0) node[above left]{83};
%\draw (35.7027, 83.13) node[above left]{DE};
% Danemark
\draw (4.3100, 5.627) node{$\times$};
%\draw (4.3100, 5.627) node[above left]{DK};
% Estonie
\draw (4.5227, 1.316) node{$\times$};
%\draw (4.5227, 1.316) node[above left]{EE};
% Grèce
\draw (13.2000, 10.993) node{$\times$};
%\draw (13.2000, 10.993) node[above left]{EL};
% Espagne
\draw (50.5990, 47.385) node{$\times$};
%\draw (50.5990, 47.385) node[above left]{ES};
% Finlande
\draw[red] (33.7100, 5.451) node{$\times$};
\draw[red] (33.7100, 5.451) node[above left]{FI};
% France
\draw (63.2733, 67.8) node{$\times$};
%\draw (63.2733, 67.8) node[above left]{FR};
% Croatie
\draw (5.6642, 4.246) node{$\times$};
%\draw (5.6642, 4.246) node[above left]{HR};
% Hongrie
\draw (9.3032, 9.879) node{$\times$};
%\draw (9.3032, 9.879) node[above left]{HU};
% Irlande 
\draw (7.0300, 4.604) node{$\times$};
%\draw (7.0300, 4.604) node[above left]{IE};
% Italie
\draw (30.1300, 60.783) node{$\times$};
%\draw (30.1300, 60.783) node[above left]{IT};
% Lituanie
\draw (6.5300, 2.943) node{$\times$};
%\draw (6.5300, 2.943) node[above left]{LT};
% Lettonie
\draw (6.4597, 2.001) node{$\times$};
%\draw (6.4597, 2.001) node[above left]{LV};
% Pays-Bas
\draw[red] (4.1200, 16.829) node{$\times$};
\draw[red] (4.1200, 16.829) node[above right]{NL};
% Pologne
\draw (31.2678, 38.496) node{$\times$};
%\draw (31.2678, 38.496) node[above left]{PL};
% Portugal
\draw (9.2400, 10.427) node{$\times$};
%\draw (9.2400, 10.427) node[above left]{PT};
% Roumanie 
\draw (23.8391, 19.943) node{$\times$};
%\draw (23.8391, 19.943) node[above left]{RO};
% Suède
\draw[red] (45.0000, 9.645) node{$\times$};
\draw[red] (45.0000, 9.645) node[above right]{SE};
% Slovénie
\draw (2.0273, 2.061) node{$\times$};
%\draw (2.0273, 2.061) node[above left]{SI};
% Slovaquie
\draw (4.9035, 5.416) node{$\times$};
%\draw (4.9035, 5.416) node[above left]{SK};
\end{tikzpicture}

\begin{enumerate}
  \item \emph{L'Allemagne correspond au point de plus haut sur le graphique. Donner la superficie et la population de ce pays, avec la précision permise par le graphique (attention aux unités).}

    D'après le graphique, la population est d'environ 83 millions d'habitants, et sa superficie d'environ 360 milliers de \unit{km^2}.
  \item \begin{enumerate}
      \item \emph{En utilisant les données suivantes, placer les points correspondant à la Finlande, la Suède et les Pays-bas.}

\begin{center}
\begin{tabular}{cln{6}{0}n{2}{3}}
\toprule
     &       & {Superficie}    & {Habitants}  \\
Code & Pays  & {(\unit{km^2})} & {(millions)} \\
\midrule
%AT & Autriche & 83871 & 8.508 \\
%BE & Belgique & 30500 & 11.204 \\
%BG & Bulgarie & 110910 & 7.246 \\
%CZ & République tchèque  & 78870 & 10.512 \\
%DE & Allemagne & 357027 & 83.13 \\
%DK & Danemark & 43100 & 5.627 \\
%EE & Estonie & 45227 & 1.316 \\
%EL & Grèce & 132000 & 10.993 \\
%ES & Espagne & 505990 & 47.385 \\
FI & Finlande & 337100 & 5.451 \\
%FR & France & 632733 & 67.8 \\
%HR & Croatie & 56642 & 4.246 \\
%HU & Hongrie & 93032 & 9.879 \\
%IE & Irlande  & 70300 & 4.604 \\
%IT & Italie & 301300 & 60.783 \\
%LT & Lituanie & 65300 & 2.943 \\
%LV & Lettonie & 64597 & 2.001 \\
NL & Pays-Bas & 41200 & 16.829 \\
%PL & Pologne & 312678 & 38.496 \\
%PT & Portugal & 92400 & 10.427 \\
%RO & Roumanie  & 238391 & 19.943 \\
SE & Suède & 450000 & 9.645 \\
%SI & Slovénie & 20273 & 2.061 \\
%SK & Slovaquie & 49035 & 5.416 \\
\bottomrule
\end{tabular}
\end{center}

Voir sur le graphique.
\item \emph{Vous remarquez que le point des Pays-Bas est plus haut des points des pays de superficie similaire, et les points de la Suède et la Finlande sont plus bas que ceux des pays de superficie similaire. Que pouvez-vous en déduire sur chacun de ces trois pays ?}

  Plusieurs pays sur une même ligne \emph{vertical} ont la même superficie, mais des populations différentes : un pays plus haut aura une population plus grande sur une même superficie, donc une population plus dense (plus d'habitants au kilomètre carré, comme en ville), alors qu'un pays plus bas aura une population plus petite sur une même superficie, donc une population moins dense (moins d'habitants au kilomètre carré, comme à la campagne, voire dans une zone quasi-désertique).

  Donc les Pays-Bas ont une population plus dense que les pays de superficie similaire ; et la Suède et la Finlande ont une population moins dense que les pays de superficie similaire.
    \end{enumerate}
\end{enumerate}
\end{exercice}

\end{document}
