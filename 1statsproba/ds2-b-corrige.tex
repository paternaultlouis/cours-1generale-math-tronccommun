%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2020-2024 Louis Paternault --- http://ababsurdo.fr
%
% Publié sous licence Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
% http://creativecommons.org/licenses/by-sa/4.0/deed.fr
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Pour compiler :
%$ lualatex $basename

\documentclass[12pt]{article}

\usepackage[
  a5paper,
  includehead,
  margin=7mm,
  headsep=3mm,
]{geometry}

\usepackage{2223-pablo}
\usepackage{2223-pablo-paternault}
\usepackage{2223-pablo-math}
\usepackage{2223-pablo-tikz}

\usepackage{2223-pablo-header}
\fancyhead[L]{\textsc{Math 1\iere{} tronc commun}}
\fancyhead[R]{\textsc{DS \no2 --- B --- Corrigé}}
\fancyfoot[C]{}

\begin{document}

\begin{exercice}
\begin{em}
Un pays fait face à une épidémie, pour laquelle un vaccin a finalement été développé. Mais de nombreuses personnes contestent son efficacité en faisant remarquer que, parmi les personnes hospitalisées pour cette maladie, près d'une sur deux a été vaccinée.

On prend une personne au hasard dans la population, et on considère les évènements suivants :
\begin{itemize}
\item $H$ : la personne est hospitalisée à cause de la maladie.
\item $V$ : la personne est vaccinée.
\end{itemize}

En utilisant des statistiques des hôpitaux, et celles des centres de vaccination, on sait que :
\begin{itemize}[$\bullet$]
\item 3\% de la population est actuellement à l'hôpital à cause de cette maladie ;
\item parmi les personnes hospitalisées des suites de cette maladie, 52\% est vaccinée ;
\item parmi les personnes qui ne sont pas hospitalisées à cause de cette maladie, 84\% est vaccinée.
\end{itemize}

Toutes les réponses seront arrondies à $10^{-4}$ près.
\end{em}

\begin{enumerate}
\item \emph{Recopier et compléter l'arbre de probabilités ci-dessous.}

Les valeurs en \textcolor{blue}{bleu} sont tirées de l'énoncé, les valeurs en \textcolor{red}{rouge} ont été obtenues par des calculs.
\begin{center}
  \begin{tikzpicture}[grow=right, sloped]
        % Set the overall layout of the tree
    \tikzstyle{level 1}=[level distance=2.5cm, sibling distance=10ex]
    \tikzstyle{level 2}=[level distance=2.5cm, sibling distance=5ex]
    \node {}
    child {
      node {$\overline{H}$}
      child {
        node {$\overline{V}$}
        edge from parent node[midway, below]{\textcolor{red}{$0,16$}}
      }
      child {
        node {$V$}
        edge from parent node[midway, above]{$0,84$}
      }
      edge from parent node[midway, below]{\textcolor{red}{$0,97$}}
    }
    child {
      node {$H$}
      child {
        node {$\overline{V}$}
        edge from parent node[midway, below]{\textcolor{red}{$0,48$}}
      }
      child {
        node {$V$}
        edge from parent node[midway, above]{\textcolor{blue}{$0,52$}}
      }
      edge from parent node[midway, above]{\textcolor{blue}{$0,03$}}
    };
  \end{tikzpicture}
\end{center} 
\item \emph{Décrire l'évènement $H\cap V$ et calculer sa probabilité.}

$H\cap V$ est l'évènement \enquote{la personne choisie est hospitalisée et vaccinée}. Sa probabilité est :
\begin{align*}
P\left(H\cap V\right)
&= P(H)\times P_H(V)\\
&= 0,03\times0,52\\
&=0,0156
\end{align*}
\item \emph{Montrer que $P(V)=0,8304$.}

\begin{align*}
P\left(V\right)
&= P\left(V\cap H\right) + P\left(V\cap \overline{H}\right)\\
&= 0,0156 + P\left(\overline{H}\right)\times P_{\overline{H}}\left(V\right)\\
&= 0,0156 + 0,97\times0,84\\
&= 0,8304
\end{align*}
\item \emph{En déduire $P_V(H)$.}
\[
P_V(H)=\frac{P\left(V\cap H\right)}{P(V)}=\frac{0,0156}{0,8304}\approx0,0188
\]
\item \emph{On admet que $P_{\overline{V}}(H)=0,0849$. Le vaccin permet-il de réduire la probabilité d'une personne d'être hospitalisée à cause de cette maladie ? Justifier.}

On a $P_{\overline{V}}(H)=0,0846$ et $P_{V}(H)=0,0188$, donc la probabilité pour une personne vaccinnée d'être hospitalisée est bien inférieure à la probabilité pour une personne non vaccinnée d'être hospitalisée. Donc le vaccin est efficace.
\end{enumerate}
\end{exercice}

\begin{exercice}
\begin{em}
  Afin de lutter contre une chenille s'attaquant à une plante, on a développé un insecticide dont on cherche à évaluer l'efficacité. On a planté un grand nombre de ces plantes, dont certaines ont été traitées avec l'insecticide, et d'autres non. On a ensuite observé lesquelles étaient attaquées par la chenille. Les résultats ont été consignés dans le tableau suivant.

      \[\begin{array}{r*{3}{|n{3}{0}}}
          \toprule
          & \multicolumn{2}{c|}{\text{Attaquées par }} & \\
          & \multicolumn{2}{c|}{\text{la chenille}} & \\
          &\text{~~Oui~~}&\text{~~Non~~}&\text{Total}\\
          \midrule
          \text{Avec insecticide} & 24 & 36 & 60 \\
          \midrule
          \text{Sans insecticide} & 72 & 108 & 180 \\
          \midrule
          \text{Total} & 96 & 144 & 240 \\
          \bottomrule
      \end{array}\]

      On choisit une plante au hasard, et on note les évènements suivants :

  \begin{itemize}
    \item $I$ : la plante a été traitée avec l'insecticide ;
    \item $C$ : la plante a été attaquée par la chenille.
  \end{itemize}
\end{em}

  \begin{enumerate}
    \item \emph{Quelle est la probabilité qu'une plante choisie au hasard ait été traitée à l'insecticide et attaquée par une chenille ?}

    Il y a 240 plantes au total, dont 24 traitée avec l'innsecticide et attaquées par la chenille, donc $P(I\cap C)=\frac{24}{240}=0,10$.
    \item \emph{Calculer $P(C)$.}
    \[P(C)=\frac{96}{240}=0,4\]

    \item \emph{Exprimer par une phrase, et calculer la probabilité $P_I(C)$.}

    $P_I(C)$ est la probabilité que la plante soit attaquée par la chenille sachant qu'elle a été traitée à l'insecticide.

    \[
    P_I(C)=\frac{\card\left({I\cap C}\right)}{\card\left({I}\right)}=\frac{24}{60}=0,4
    \]

    \item \emph{Les évènements $C$ et $I$ sont-ils indépendants ? Justifier.}

    On remarque, d'après les calculs précédents, que $P(C)=P_I(C)$, donc les évènements sont indépendants.
    \item \emph{L'insecticide est-il efficace ? Justifier.}

    Nous venons de montrer que les évènements $I$ et $C$ sont indépendants, ce qui veut dire que le fait qu'il y ait ou non de l'insecticide n'a aucune influence sur la probabilité d'une plante de se faire attaquer par les chenilles : l'insecticide est inefficace.
  \end{enumerate}
\end{exercice}

\end{document}
