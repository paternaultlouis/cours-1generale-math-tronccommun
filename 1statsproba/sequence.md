# Statistiques et Probabilités

## Information chiffrée (1 ou 2 semaines)

- Cours
  - Lecture de tableaux croisés d'effectifs / nuage de points / diagramme en barres / diagramme circulaire
  - Écriture de tableaux croisés d'effectifs / nuage de points / diagramme en barres / diagramme circulaire
- Exercices : Plan de travail
- Informatique :
    - Filtres
    - Diagrammes : nuages de points / diagramme en barres / diagramme circulaire

## Probabilités conditionnelles

TODO Déplacer les fréquences marginales et conditionnelles dans la partie précédente

TODO

TODO saucissoner davantage, pour alterner cours et plan de travail
