- Calculer un taux d'évolution
  https://www.youtube.com/watch?v=Y48-iK7Cp20

- Appliquer un taux d'évolution
  https://www.youtube.com/watch?v=UVXFEDUnSjI

- Taux d'évolutions successifs
  https://www.youtube.com/watch?v=qOg2eXd8Hv0

- Taux d'évolution réciproque
  https://www.youtube.com/watch?v=NiCxHYkpNiM
